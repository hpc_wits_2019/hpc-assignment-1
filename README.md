# HPC Assignmet 1 - Parallel KNN and Search

## Dependencies 
* `sudo apt install make`
* `sudo apt install gcc`

## Running Programs 
### Problem 1
* navigate to `cd '<path_to_containing_folder>/hpc-assignment-1/problem 1`
* open terminal and run `make`
* run `./kNN_base`

Results will be displayed

### Problem 2
* navigate to `cd '<path_to_containing_folder>/hpc-assignment-1/problem 2`
* open terminal and run `make`
* run `./prob_2`

Results will be displayed

# Report 
see Report.pdf


# Problem Description 
see assignment1.pdf <br>
<br> 
![objectives](https://i.gyazo.com/1e0155d32c6c1f950e5a75d69ed46cd9.png)

## 1

![Problem 1](https://i.gyazo.com/1837040b05f109dfa827aaed242be1fd.png)
![](https://i.gyazo.com/6935bdd6d3597c6850cf4437b67893fe.png)
![](https://i.gyazo.com/eed56b442ef81f465487589f6d818160.png)

## 2

![Problem 2](https://i.gyazo.com/be4dd3c01feaab6d9c42bb45908f276e.png)
