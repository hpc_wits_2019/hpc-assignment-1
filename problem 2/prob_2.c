#include <stdio.h>
#include <time.h>   //need the time to generate psuedoranom numbers
#include <stdlib.h> //header file for rand() function
#include <math.h>   //need this for rounding and perhaps other maths...

#include <omp.h>    //parallel openMP

float frand ( float low, float high );
void scalarMatrixMultiplication(float scalar,float** matrix,float** out, int k, int l, int i, int j);
void scalarVectorMultiplication (float scalar,float* vector,float** out, int oRow, int j,int col);

int main(){

//#############################
//######### PART 1 ############
//#############################

//first matrix A
int i = 3;
int j = 4;
//second matrix B
int k = 5;
int l = 6;

printf("i = %d\n", i);
printf("j = %d\n", j);
printf("k = %d\n", k);
printf("l = %d\n\n", l);

printf("calculating...\n\n");

//Initialize matricies
//A
float** mA = malloc(sizeof(float*)*i);
for (size_t a = 0; a < i; a++) {
  mA[a] = malloc(sizeof(float)*j);
}

//B
float** mB = malloc(sizeof(float*)*k);
for (size_t a = 0; a < k; a++) {
  mB[a] = malloc(sizeof(float)*l);
}


//Output (IxK x JxL)
float** mOut = malloc(sizeof(float*)*(i*k));
for (size_t a = 0; a < (i*k); a++) {
  mOut[a] = malloc(sizeof(float)*(j*l));
}


//generate synthetic data
srand(time(NULL));

//A
  for (int a = 0; a < i; a++){
  for (int b = 0; b < j; b++){
    mA[a][b] = frand(5,1);
    // printf("\n mA[%d][%d] = %f ",a,b, mA[a][b]);
  }
}

//B
for (int c = 0; c < k; c++){
for (int d = 0; d < l; d++){
  mB[c][d] = frand(5,1);
  // printf("\n mB[%d][%d] = %f ",c,d, mB[c][d]);
}
}


float timer;
float t1 = omp_get_wtime();
//#pragma omp parallel for
// calculation
for (size_t a = 0; a < i; a++) {
  for (size_t b = 0; b < j; b++) {
    scalarMatrixMultiplication(mA[a][b],mB,mOut,k, l,a,b); //for a scalar at mA[a][b] multiply by the entire matrix B
  }
}

float t2 = omp_get_wtime();
timer = t2 -t1;

printf("\n*Kronecker product*\n\n");
//print 'out' matrix
for (size_t a = 0; a < i*k; a++) {
  for (size_t b = 0; b < j*l; b++) {
  printf("mOut[%lu][%lu]: %f \n",a,b,mOut[a][b]);
  }
}

printf("\n*Expected dimensions* %d x %d\n\n",i*k,j*l);

printf("\nElapse time in SERIAL: %f\n\n",timer);


//##########################
//######## PART 2 ##########
//##########################

//Initialize matricies
//A2 (I X K)
float** mA2 = malloc(sizeof(float*)*i);
for (size_t a = 0; a < i; a++) {
  mA2[a] = malloc(sizeof(float)*k);
}

//B2 (J X K)
float** mB2 = malloc(sizeof(float*)*j);
for (size_t a = 0; a < j; a++) {
  mB2[a] = malloc(sizeof(float)*k);
}


//Output2 (IxJ x K)
float** mOut2 = malloc(sizeof(float*)*(i*j));
for (size_t a = 0; a < (i*j); a++) {
  mOut2[a] = malloc(sizeof(float)*(k));
}
//
float* colV = malloc(sizeof(float)*(j));

//generate synthetic data
//A2
  for (int a = 0; a < i; a++){
  for (int b = 0; b < k; b++){
    mA2[a][b] = frand(5,1);
    // printf("\n mA[%d][%d] = %f ",a,b, mA[a][b]);
  }
}

//B2
for (int c = 0; c < j; c++){
for (int d = 0; d < k; d++){
  mB2[c][d] = frand(5,1);
  // printf("\n mB[%d][%d] = %f ",c,d, mB[c][d]);
}
}


// calculation
t1 = omp_get_wtime();
//#pragma omp parallel for
 for (size_t col = 0; col < k; col++){
  for (size_t oRow = 0; oRow < i; oRow++){
//get the column vector of B
for (size_t iRow = 0; iRow < j; iRow++) {
  colV[iRow] = mB2[iRow][col];
}
    scalarVectorMultiplication(mA2[oRow][col],colV,mOut2,oRow,j,col); //for a scalar at mA[a][b] multiply by the entire matrix B
  }
}
t2 = omp_get_wtime();
timer = t2 -t1;

printf("\n*Khatri-Rao product*\n\n");
//print 'out' matrix
for (size_t a = 0; a < i*j; a++) {
  for (size_t b = 0; b < k; b++) {
  printf("mOut2[%lu][%lu]: %f \n",a,b,mOut2[a][b]);
  }
}
printf("\n*Expected dimensions* %d x %d\n\n",i*j,k);

printf("\nElapse time in SERIAL: %f\n\n",timer);



return 0;
}


//####### FUNCTIONS ########


void scalarMatrixMultiplication(float scalar,float** matrix,float** out, int k, int l,int i, int j)
{
  for (size_t a = 0; a < k ; a++) {
    for (size_t b = 0; b < l; b++) {
      *(*(out+a+(k*i))+b+(l*j)) = scalar * matrix[a][b];
    }
  }
}

float frand ( float low, float high ){
    return ( (float)rand() * ( high - low ) ) / (float)RAND_MAX + low;
}

void scalarVectorMultiplication (float scalar,float* vector,float** out, int oRow, int j,int col)
{

    for (size_t a = 0; a < j; a++) {
      *(*(out+a+(oRow*j))+col) = scalar * vector[a];
      //printf("Out: %f\n",scalar*vector[a] );
    }
  }






//
